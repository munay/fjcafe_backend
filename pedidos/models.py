from django.db import models
from django.utils import timezone


class Pedido(models.Model):
    cliente = models.CharField(max_length=100)
    mesa = models.IntegerField()
    lista_productos = models.JSONField()
    estado = models.CharField(max_length=20, default='pendiente')
    fecha_inicio = models.DateTimeField(auto_now_add=True)
    fecha_fin = models.DateTimeField(null=True, blank=True)

    # def save(self, *args, **kwargs):
    #     if self.estado == 'listo' and not self.fecha_fin:
    #         self.fecha_fin = timezone.now()
    #     super().save(*args, **kwargs)

    def __str__(self) -> str:
        return str(self.mesa)